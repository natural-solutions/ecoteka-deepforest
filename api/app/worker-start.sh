#! /usr/bin/env bash
set -e

celery worker -A app.worker -l debug -Q main-queue -c 1
