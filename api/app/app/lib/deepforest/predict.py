from deepforest import (
    main,
    preprocess
)
import numpy
import pandas
import torch
from torchvision.ops import nms
from typing import List


class Model:

    def __init__(self):
        self.model = main.deepforest()
        self.model.use_release()

    def predict_image(
            self,
            numpy_image: numpy.ndarray) -> pandas.DataFrame:
        """
        Use the model to make a prediction on a numpy array 
        (type=float32)
        """
        numpy_rgb = Model.check_rgb(numpy_image)
        df_predictions = self.model.predict_image(
            image=numpy_rgb.astype('float32'),
            path=None,
            return_plot=False)
        return df_predictions

    def process_windows(
            self,
            image: numpy.ndarray,
            patch_size: float,
            patch_overlap: float) -> List:
        """
        Create a sliding window object from an image.
        """
        Model.check_rgb(image)
        return preprocess.compute_windows(
            image,
            patch_size,
            patch_overlap
        )

    def drop_alpha(numpy_image: numpy.ndarray) -> numpy.ndarray:
        """
        Drop the alpha channel from a rgba numpy image
        """
        return numpy_image[:, :, :3]

    def check_rgb(numpy_image: numpy.ndarray) -> numpy.ndarray:
        """
        Check if the image is rgb and not rgba
        """
        if numpy_image.shape[2] == 4:
            return Model.drop_alpha(numpy_image)
        return numpy_image

    def nms(self, predicted_boxes: List) -> pandas.DataFrame:
        """
        Make non-max-suppression over the predicted_boxes
        """
        predicted_boxes = pandas.concat(predicted_boxes)

        boxes = torch.tensor(
            predicted_boxes[["xmin", "ymin", "xmax", "ymax"]].values,
            dtype=torch.float32
        )

        scores = torch.tensor(
            predicted_boxes.score.values,
            dtype=torch.float32
        )

        labels = predicted_boxes.label.values

        bbox_left_idx = nms(
            boxes=boxes,
            scores=scores,
            iou_threshold=0.15
        )

        bbox_left_idx = bbox_left_idx.numpy()
        new_boxes, new_labels, new_scores = boxes[bbox_left_idx].type(
            torch.int), labels[bbox_left_idx], scores[bbox_left_idx]

        image_detections = numpy.concatenate(
            [
                new_boxes,
                numpy.expand_dims(new_labels, axis=1),
                numpy.expand_dims(new_scores, axis=1)
            ],
            axis=1
        )

        mosaic_df = pandas.DataFrame(
            image_detections,
            columns=[
                "xmin",
                "ymin",
                "xmax",
                "ymax",
                "label",
                "score"
            ]
        )

        return mosaic_df
