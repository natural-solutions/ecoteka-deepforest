from app.api.api_v1.deps import (
    add_image_id_to_prediction,
    convert_predict,
    get_image_from_minio,
    polygon_from_dict,
    transform_boxes
)
from app.core.celery_app import celery_app
from app.core.model import model
from app.core.ws import broadcast
from app.schemas import PredictionOut
from asgiref.sync import async_to_sync
from celery import current_task
import io
import torch
from tqdm import tqdm


# el gran Mathias se acaba de ganar una cerverza enorme !!! :D
torch.set_num_threads(1)


async def publish(channel: str, string_io: io.StringIO) -> None:
    string_io.seek(0)
    await broadcast.publish(
        channel=channel,
        message=string_io.read()
    )
    string_io.seek(0)
    string_io.truncate(0)


async def prediction(
        task_id: str,
        image_id: str,
        polygon_data: dict) -> PredictionOut:

    numpy_image = get_image_from_minio(image_id)
    polygon = polygon_from_dict(polygon_data)

    await broadcast.connect()
    output = io.StringIO()

    windows = model.process_windows(
        numpy_image,
        500,
        0.15
    )

    predicted_boxes = []

    for index, window in enumerate(tqdm(windows, file=output)):

        await publish(f"tasks/{task_id}", output)

        crop = numpy_image[windows[index].indices()]
        boxes = model.predict_image(crop)

        if boxes is not None:

            xmin, ymin, xmax, ymax = windows[index].getRect()
            predicted_boxes.append(
                transform_boxes(boxes, [xmin, ymin])
            )

    nb_predictions = len(predicted_boxes)

    if nb_predictions == 0:
        return None
    elif nb_predictions == 1:
        df_predictions = predicted_boxes[0]
    else:
        df_predictions = model.nms(predicted_boxes)

    await publish(f"tasks/{task_id}", output)

    await broadcast.disconnect()

    prediction_process = convert_predict(df_predictions, polygon)

    return add_image_id_to_prediction(
        prediction_process,
        image_id
    )


@celery_app.task(acks_late=True)
def predict_task(
        image_id: str,
        polygon_data: dict) -> PredictionOut:
    """
    Make a prediction from an image minio id and the dict of a polygon.
    """
    return async_to_sync(prediction)(
        task_id=current_task.request.id,
        image_id=image_id,
        polygon_data=polygon_data
    )
