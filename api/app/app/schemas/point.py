from pydantic import BaseModel
from pydantic.main import Extra


class PointBase(BaseModel):
    x: int
    y: int


class Point(PointBase, extra=Extra.forbid):
    pass
