import json
from .point import Point
from pydantic import BaseModel
from pydantic.main import Extra
from typing import List


class PolygonBase(BaseModel):
    points: List[Point]


class Polygon(PolygonBase, extra=Extra.forbid):
    pass

    @classmethod
    def __get_validators__(cls):
        yield cls.validate_to_json

    @classmethod
    def validate_to_json(cls, value):
        if isinstance(value, str):
            return cls(**json.loads(value))
        return value
