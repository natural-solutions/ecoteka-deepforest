from .detection import Detection
from .polygon import Polygon
from typing import List

from pydantic import BaseModel


class PredictionBase(BaseModel):
    polygon: Polygon


class PredictionProcess(PredictionBase):
    detections: List[Detection]
    total: int
    total_inside: int


class PredictionOut(PredictionProcess):
    image_id: str
