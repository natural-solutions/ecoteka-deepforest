from .point import Point
from pydantic import BaseModel


class DetectionBase(BaseModel):
    origin: Point
    width: int
    height: int
    score: float
    label: str
    inside: bool


class Detection(DetectionBase):
    pass
