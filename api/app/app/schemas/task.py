from pydantic import BaseModel


class TaskBase(BaseModel):
    task_id: str
    image_id: str
    message: str


class Task(TaskBase):
    pass
