from broadcaster import Broadcast
from app.core.config import settings

broadcast = Broadcast(settings.WEBSOCKET_BACKEND)
