from app.core.config import settings
from minio import Minio

minio_client = Minio(
    settings.MINIO_HOST,
    secure=settings.MINIO_SECURE,
    access_key=settings.MINIO_USER,
    secret_key=settings.MINIO_PASSWORD,
)

found = minio_client.bucket_exists(settings.MINIO_BUCKET)

if not found:
    minio_client.make_bucket(settings.MINIO_BUCKET)
