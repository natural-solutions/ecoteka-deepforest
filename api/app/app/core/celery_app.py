from app.core.config import settings
from celery import Celery

celery_app = Celery(
    "worker",
    broker=settings.WORKER_BROKER,
    backend=settings.WORKER_BACKEND
)
celery_app.conf.task_routes = {
    "app.worker.predict_task": "main-queue"
}
