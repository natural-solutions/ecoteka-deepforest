from pydantic import AnyHttpUrl, BaseSettings, validator
from typing import List, Union


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    SERVER_NAME: str
    SERVER_HOST: AnyHttpUrl
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []
    WORKER_BROKER: str
    WORKER_BACKEND: str

    MINIO_HOST: str = 'minio:9000'
    MINIO_SECURE: bool = False
    MINIO_USER: str = 'minio'
    MINIO_PASSWORD: str = 'minio123'
    MINIO_BUCKET: str = 'deepforest'

    WEBSOCKET_BACKEND: str = 'redis://redis:6379/1'

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(
            cls,
            v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    PROJECT_NAME: str

    class Config:
        case_sensitive = True


settings = Settings()
