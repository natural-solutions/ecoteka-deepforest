from app.core.ws import broadcast
from fastapi import APIRouter
from fastapi.websockets import WebSocket
from starlette.concurrency import run_until_first_complete


async def tasks_receiver(websocket: WebSocket, task_id: str):
    async for message in websocket.iter_text():
        await broadcast.publish(
            channel=f"tasks/{task_id}",
            message=message
        )


async def tasks_sender(websocket: WebSocket, task_id: str):
    async with broadcast.subscribe(
            channel=f"tasks/{task_id}") as subscriber:
        async for event in subscriber:
            await websocket.send_text(event.message)

router = APIRouter()


@router.websocket("/tasks/{task_id}")
async def tasks(
    websocket: WebSocket,
    task_id: str,
):
    await websocket.accept()
    await run_until_first_complete(
        (
            tasks_receiver,
            {"websocket": websocket, "task_id": task_id}
        ),
        (
            tasks_sender,
            {"websocket": websocket, "task_id": task_id}
        )
    )
