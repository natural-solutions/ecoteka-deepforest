from app.core.celery_app import celery_app
from fastapi import APIRouter
from fastapi.responses import JSONResponse


router = APIRouter()


@router.get("/{task_id}")
def get_status(task_id):
    """"
    Return informations about the requested task
    """
    task_result = celery_app.AsyncResult(task_id)
    result = {
        "id": task_id,
        "status": task_result.status,
        "result": task_result.result
    }

    if result['status'] == 'FAILURE':
        result['result'] = str(task_result.result)

    return JSONResponse(result)
