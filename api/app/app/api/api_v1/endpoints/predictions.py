from app.schemas import (
    Task,
    Polygon
)
from app.api.api_v1.deps import (
    polygon_is_valid,
    save_image_to_minio,
    polygon_to_dict,
    upload_is_image
)
from app.core.celery_app import celery_app
from fastapi import (
    APIRouter,
    UploadFile,
    HTTPException,
    Body,
    File
)
import re
from starlette import datastructures
import tempfile
from typing import Optional
from urllib.request import urlopen


router = APIRouter()


@router.post("/image", response_model=Task)
async def generate_prediction_image(
        upload_image: UploadFile = File(...),
        polygon: Optional[Polygon] = Body(None)) -> Task:
    """
    Return the prediction of the model from an RGB image.
    """

    if not (await upload_is_image(upload_image)):
        raise HTTPException(
            status_code=415,
            detail="Upload file is not an image"
        )

    image_id = await save_image_to_minio(upload_image)
    polygon_data = polygon_to_dict(polygon)

    if not polygon_is_valid(polygon_data):
        raise HTTPException(
            status_code=415,
            detail="Invalid Polygon"
        )

    task = celery_app.send_task(
        'app.worker.predict_task',
        [image_id, polygon_data]
    )

    return {
        'task_id': task.id,
        'image_id': image_id,
        'message': f"Your task id is {task.id}"
    }


@router.post("/url", response_model=Task)
async def generate_prediction_url(
        image_url: str,
        polygon: Optional[Polygon] = Body(None)) -> Task:
    """
    Return the prediction of the model from the url an RGB image.
    """

    try:
        http = urlopen(image_url)
    except Exception:
        raise HTTPException(
            status_code=415,
            detail="Url can't be access"
        )

    header_disposition = http.getheader('Content-Disposition')
    header_type = http.getheader('Content-Type')

    if (header_disposition is None
            and header_type is None):
        raise HTTPException(
            status_code=415,
            detail="Http response header must have either "
            "Content-Disposition or Content-Type"
        )

    if header_disposition is None:
        content_type = header_type
        extension = content_type.split('/')[-1]
        filename = 'tmp.' + extension
    else:
        filename = re.search(
            'filename=.*',
            header_disposition
        ).group()[10:-1]
        if header_type is None:
            content_type = ""
        else:
            content_type = header_type

    file = tempfile.SpooledTemporaryFile()
    file.write(http.read())

    upload = datastructures.UploadFile(
        filename,
        file,
        content_type
    )

    return await generate_prediction_image(
        upload,
        Polygon(**polygon)
    )
