from app.api.api_v1.endpoints import predictions, tasks, ws
from fastapi import APIRouter


api_router = APIRouter()
api_router.include_router(
    predictions.router,
    prefix="/predictions",
    tags=["predictions"]
)
api_router.include_router(
    tasks.router,
    prefix="/tasks",
    tags=["tasks"]
)
api_router.include_router(
    ws.router,
    prefix="/ws",
    tags=["ws"]
)
