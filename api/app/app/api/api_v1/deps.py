from app.core.config import settings
from app.core.minio import minio_client
from app.schemas import (
    Polygon,
    PredictionProcess,
    PredictionOut,
    Detection
)
from fastapi.datastructures import UploadFile
import filetype
from imagehash import average_hash
import io
import numpy
import pandas
from PIL import Image
from shapely import geometry
from typing import List


def polygon_is_valid(polygon: Polygon) -> bool:
    """
    Check if a Polygon is valid (vertices do not cross each other)
    """
    list_points = points_to_list(polygon)
    if len(list_points) < 3:
        return True
    polygon_geometry = geometry.Polygon(list_points)
    return polygon_geometry.is_valid


def polygon_to_dict(polygon: Polygon) -> dict:
    """
    Convert a Polygon to a dict and check if None
    """
    if polygon is None:
        return None
    return polygon.dict()


def polygon_from_dict(polygon_data: dict) -> Polygon:
    """
    Convert a dict to a Polygon and check if None
    """
    if polygon_data is None:
        return None
    return Polygon(**polygon_data)


def image_from_bytes(data: bytes) -> Image:
    buffer = io.BytesIO(data)
    return Image.open(buffer)


async def read_upload(upload: UploadFile) -> bytes:
    await upload.seek(0)
    file = await upload.read()
    await upload.seek(0)
    return file


async def upload_is_image(upload: UploadFile) -> bool:
    """
    Check if the upload is an image
    """
    file = await read_upload(upload)
    return filetype.is_image(file)


async def save_image_to_minio(upload_image: UploadFile) -> str:
    """
    Save an image to minio server and return the id
    """
    image = image_from_bytes(await read_upload(upload_image))
    img_hash = average_hash(image)
    extension = upload_image.filename.split('.')[-1]
    id = f"{img_hash}.{extension}"

    minio_client.fput_object(
        settings.MINIO_BUCKET,
        id,
        upload_image.file.fileno()
    )

    return id


def get_image_from_minio(image_id: str) -> numpy.ndarray:
    """
    Load an image from minio server with an id
    """
    try:
        response = minio_client.get_object(
            settings.MINIO_BUCKET,
            image_id
        )
        image = image_from_bytes(response.read())
    finally:
        response.close()
        response.release_conn()
    return numpy.asarray(image)


def dataframe2detection(dataframe: pandas.DataFrame) -> Detection:
    """
    Convert the DataFrame with the boxes into a list of Detections
    """
    detections = []
    for box in dataframe.values:
        detections.append(
            {
                'origin': {
                    'x': box[0],
                    'y': box[1]
                },
                'width': box[2] - box[0],
                'height': box[3] - box[1],
                'score': box[5],
                'label': box[4],
                'inside': False
            }
        )
    return detections


def process_prediction(
        detections: List[Detection],
        polygon_data: Polygon) -> PredictionProcess:
    """
    Create a PredictionProcess
    """
    if polygon_data is not None:
        polygon = polygon_data.dict()
    else:
        polygon = {"points": []}
    polygon_preprocess = {
        'detections': detections,
        'total': len(detections),
        'polygon': polygon,
        'total_inside': 0
    }
    polygon_process = update_total_inside(polygon_preprocess)
    return polygon_process


def convert_polygon(data: str) -> Polygon:
    """
    Parse a string into a Polygon (dict)
    """
    polygon = Polygon.parse_raw(data)
    return polygon


def points_to_list(polygon: Polygon) -> List[List[int]]:
    """
    Convert a Polygon into a list of coord (list of integer)
    """
    polygon_list = []
    for point in polygon['points']:
        polygon_list.append(
            [(int)(point['x']),
             (int)(point['y'])])
    return polygon_list


def update_total_inside(
        prediction: PredictionProcess) -> PredictionProcess:
    """
    Check if the Prediction has a not empty Polygon and if so check 
    and count how many Detections are inside
    """
    if len(prediction['polygon']['points']) < 3:
        return {
            'detections': prediction['detections'],
            'total': prediction['total'],
            'polygon': prediction['polygon'],
            'total_inside': 0
        }
    total_inside = 0
    zone = geometry.Polygon(points_to_list(prediction['polygon']))
    detections = []
    for detection in prediction['detections']:
        xmin = detection['origin']['x']
        ymin = detection['origin']['y']
        xmax = xmin + detection['width']
        ymax = ymin + detection['height']
        box_points = [
            [xmin, ymin],
            [xmin, ymax],
            [xmax, ymax],
            [xmax, ymin]
        ]
        box = geometry.Polygon(box_points)
        intersection_area = box.intersection(zone).area
        box_area = box.area
        ioa = intersection_area / box_area
        if ioa > 0.4:
            detection['inside'] = True
            total_inside += 1
        detections.append(detection)
    return {
        'detections': detections,
        'total': prediction['total'],
        'polygon': prediction['polygon'],
        'total_inside': total_inside
    }


def convert_predict(
        predictions: pandas.DataFrame,
        polygon: Polygon) -> PredictionOut:
    """
    Convert model predictions and polygon to schema.PredictionOut
    """
    detections = dataframe2detection(predictions)
    return process_prediction(detections, polygon)


def add_image_id_to_prediction(
        prediction: PredictionProcess,
        image_id: int) -> PredictionOut:
    """
    Add the image_id to a PredictionProcess to make a PredictionOut
    """
    return {
        'detections': prediction['detections'],
        'total': prediction['total'],
        'polygon': prediction['polygon'],
        'total_inside': prediction['total_inside'],
        'image_id': image_id
    }


def transform_boxes(
        boxes: pandas.DataFrame,
        coords: List) -> pandas.DataFrame:
    """
    Transform the boxes cooridnates by adding coords
    """

    xmin = coords[0]
    ymin = coords[1]
    boxes.xmin = boxes.xmin + xmin
    boxes.xmax = boxes.xmax + xmin
    boxes.ymin = boxes.ymin + ymin
    boxes.ymax = boxes.ymax + ymin

    return boxes
