# ecoTeka DeepForest

ecoteka-deepforest is an API using DeepForest package (https://github.com/weecology/DeepForest) to make tree crown detection from airborn RGB imagery.

The current version use the model from the release of the DeepForest package but futur version will allow to use a custom model and also fine-tuning.

## Requirements

All work environments need at least the following requirements:

- Docker: [how to install it][docker]
- Docker Compose: [how to install it][docker-compose]


## Install

Clone git repository :

```
# Use https
git clone https://gitlab.com/natural-solutions/ecoteka-deepforest.git

# Or ssh
git clone git@gitlab.com:natural-solutions/ecoteka-deepforest.git
```

Inside the cloned repository, use docker-compose :

```
# Build the project
docker-compose build

# Launch the api
docker-compose up -d
```

## Environment variables

These are the environment variables that you can set in docker-compose in the file .env to
configure it and their default values:

### GLOBAL

| Key    | Description                             | Default value |
| :----- | :-------------------------------------- | ------------: |
| DOMAIN | The domain name associated to the stack |     localhost |

### DB

| Key                   | Description                                  | Default value |
| :-------------------- | :------------------------------------------- | ------------: |
| DOCKER_IMAGE_POSTGRES | The name of the postgres docker image.       | postgres:13.3 |
| POSTGRES_PASSWORD     | The [PostgresSQL][postgresql] user password. |      password |

### Redis

| Key                | Description                         | Default value |
| :----------------- | :---------------------------------- | ------------: |
| DOCKER_IMAGE_REDIS | The name of the redis docker image. |  redis:alpine |

### Minio

| Key                 | Description                         | Default value |
| :------------------ | :---------------------------------- | ------------: |
| DOCKER_IMAGE_MINIO  | The name of the minio docker image. |   minio/minio |
| MINIO_ROOT_USER     | The [MinIO][minio] user.            |         minio |
| MINIO_ROOT_PASSWORD | The [MinIO][minio] user password.   |      minio123 |

### Backend

| Key          | Description                              |      Default value |
| :----------- | :--------------------------------------- | -----------------: |
| PROJECT_NAME | The project name for [FastAPI][fastapi]. | ecoteka-deepforest |


### Worker

| Key            | Description                   |                                 Default value |
| :------------- | :---------------------------- | --------------------------------------------: |
| CELERY_BROKER  | The [Celery][celery] broker.  |                          redis://redis:6379/0 |
| CELERY_BACKEND | The [Celery][celery] backend. | db+postgresql://postgres:password@db/postgres |


## Access To API Documentation

The project's API documentation is found once the local instance of the
backend is started on the next path:

- http://localhost:8880/docs


## Usage

The current version was tested using images from the IGN (Institut national de l'information géographique et forestière). One way to access this images is with this project : https://github.com/aloui-mathias/pyqgis_IGN_WMTS

### Input

To make a prediction, you need three parameters :

- An aerial image with a 20 centimeters per pixel resolution
- A polygon if you only want trees in a certain area

For example you can use the image **ecoteka-deepforest/docs/example/upload_image.tiff** og the Parc Borely in Marseille, France :

<img src="docs/example/upload_image.png" alt="upload_image.png" width="800"/>

and the polygon **ecoteka-deepforest/docs/example/polygon.json** that delimits the park.

Here is a possible response for the Parc Borely in the file **ecoteka-deepforest/docs/example/predictions-response.json** :

```
{
    "task_id": "d6bb767e-9987-419f-bd66-584a5f509f3c",
    "image_id": "-5145162921467436933.tiff",
    "message": "Your task id is d6bb767e-9987-419f-bd66-584a5f509f3c"
}
```

### Task

To send these parameters, use the predictions/image/ endpoint. It will return an id that you can then use with the tasks/ endpoint to know the status of your task.

The task status is either PENDING if the task is not complete, FAILURE if there was an error or SUCCESS if it's complete.

Here is a possible response when a task is PENDING in the file **ecoteka-deepforest/docs/example/tasks-response-pending.json** :

```
{
    "id": "d6bb767e-9987-419f-bd66-584a5f509f3c",
    "status": "PENDING",
    "result": null
}
```

### Output

Once the prediction is over, you will have in the result of the task informations a list of detections which are coordinates, width, height, score and label of a possible tree. If the tree is inside the polygon passed in parameters, the value inside will be True. The task result also show the total number of tree detected, the polygon passed as parameters and the number of trees inside.

You have the final response once the task is finished for the Parc Borely in the file **ecoteka-deepforest/docs/example/tasks-response-success.json**.

## Development known issues

- With windows, if the api doesn't start and the command :
  
  ```
  docker-compose logs api
  ```

  shows :

  ```
  api_1     | Checking for script in /app/prestart.sh
  api_1     | Running script /app/prestart.sh
  : not found /start-reload.sh: 2: /app/prestart.sh:
  ```

  it might indicate a problem with the line breaks type.
  
  To solve, change the line breaks type to LF in the file **ecoteka-deepforest/api/app/prestart.sh**.

- In **ecoteka-deepforest\api\app\app\worker.py** there is a line of code :

  ```
  # el gran Mathias se acaba de ganar una cerverza enorme !!! :D
  torch.set_num_threads(1)
  ```

  This line ensures that pytorch will only use one thread. We have to do that because we are using pytorch inside a celery worker with a concurrency of one. It appears that pytorch detects the number of threads globally available and not the threads available for the worker.

# TODO

- [x] Remove model loading from api
- [x] Add endpoint to allow send an image from an url
- [x] Remove is_tile and automatically manage with the image size
- [ ] Find the relation between resolution image size and accuracy
- [ ] Add the possibility to use custom model
- [ ] Add the possibility to fine-tune a model

---

![logo-natural-solutions][logo-ns]

[logo-ns]: docs/logos/natural-solutions-logo-horizontal.png
[docker]: https://docs.docker.com/get-docker
[docker-compose]: https://docs.docker.com/compose/install
[postgresql]: https://www.postgresql.org/docs/12/index.html
[minio]: https://docs.min.io/docs/minio-quickstart-guide.html
[fastapi]: https://fastapi.tiangolo.com
[celery]: https://docs.celeryproject.org/en/stable/
